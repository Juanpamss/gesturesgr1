//
//  SwipeViewController.swift
//  Gestures
//
//  Created by Juan Pa on 17/1/18.
//  Copyright © 2018 Juan Pa. All rights reserved.
//

import UIKit

class SwipeViewController: UIViewController {

    @IBOutlet weak var customView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
    @IBAction func swipeUpAction(_ sender: Any) {
        
        customView.backgroundColor = .black
    }
    
    @IBAction func swipeDownAction(_ sender: Any) {
        
        customView.backgroundColor = .blue
    }
    
    @IBAction func swipeLeftAction(_ sender: Any) {
        
        customView.backgroundColor = .red
    }
    
    @IBAction func swipeRightAction(_ sender: Any) {
        
        customView.backgroundColor = .yellow
    }
    
}
