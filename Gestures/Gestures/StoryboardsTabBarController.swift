//
//  StoryboardsTabBarController.swift
//  Gestures
//
//  Created by Juan Pa on 23/1/18.
//  Copyright © 2018 Juan Pa. All rights reserved.
//

import UIKit

class StoryboardsTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        
        let left = tabBar.items![0]
        let right = tabBar.items![1]
        
        
        left.image = #imageLiteral(resourceName: "ic_pan_tool")
        right.image = #imageLiteral(resourceName: "ic_open_with")
        left.title = "Tap"
        right.title = "Swipe"
        
        left.badgeColor = .red
        left.badgeValue = "6"
        
    }

    

}
