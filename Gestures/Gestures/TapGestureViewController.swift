//
//  TapGestureViewController.swift
//  Gestures
//
//  Created by Juan Pa on 17/1/18.
//  Copyright © 2018 Juan Pa. All rights reserved.
//

import UIKit

class TapGestureViewController: UIViewController {

    
    @IBOutlet weak var touchesLabel: UILabel!
    
    @IBOutlet weak var tapLabel: UILabel!
    
    @IBOutlet weak var coordLabel: UILabel!
    
    @IBOutlet weak var customView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touchCount = touches.count
        let tap = touches.first
        let tapCount = tap?.tapCount
        
        touchesLabel.text = "\(touchCount ?? 0)"
        tapLabel.text = "\(tapCount ?? 0)"
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
        let point = touch?.location(in: self.view)
        
        let x = point?.x
        let y = point?.y
        
        coordLabel.text = "x: \(x!), y: \(y!)"
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        print ("Terminó!")
        
    }
    
    
    @IBAction func tapGestureAction(_ sender: Any) {

        customView.backgroundColor = .red
      
    }
    
}
